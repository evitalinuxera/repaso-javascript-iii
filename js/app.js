import {materia} from './importar.js';
import miFacu from './importar.js';
import {diaClase} from './importar.js';
import {recordar} from './importar.js';
import {Auto} from './importar.js';
import {Moto} from './importar.js';

// Comenzamos con POO (Programación Orientada a Objetos)
// Ya vimos cómo era el Object Constructor
// Ahora javasScript soporta POO
// Creamos un objeto y lo instanciamos varias veces

class Jugador { constructor (nombre, posicion) {
        this.nombre = nombre;
        this.posicion = posicion
    }
};

let jugador1 = new Jugador("Lionel Messi", "10");
let jugador2 = new Jugador("Cristiano Ronaldo", "7");
let jugador3 = new Jugador("Franco Armani", "Arquero");

console.log (jugador1);
console.log (jugador2);
console.log (jugador3);

// Ahora vamos a crear otro objeto, pero ya con un método, no solo con atributos
// Esto lo podemos ver desplegando la estructura y fijándose en Prototypes
// Antes solo figuraba la clase

class JugadorLeyenda { constructor (nombre, posicion) {
        this.nombre = nombre;
        this.posicion = posicion
    }
    mostrar() {
        return `La leyenda ${this.nombre} jugaba de ${this.posicion}`;
    }
};

let jugadorLeyenda1 = new JugadorLeyenda("Diego Maradona", "El 10 eterno");
console.log(jugadorLeyenda1);
console.log (jugadorLeyenda1.mostrar());


// No descubrimos nada si decimos que la herencia es una de las características de las
// clases. Vamos a ver cómo aplicar la herencia en javaScript
// Esto es importante porque en React lo vamos a usar mucho
// herendando métodos propios de React.
// Ojo en la construcción, no hay que traer todos los this.sarasa porque te va a dar error
// Se superponen con "super" y luego se agregan los que querramos.


class JugadorFracasado extends JugadorLeyenda {
    constructor(nombre, posicion, epoca) {
        super(nombre, posicion);
        this.epoca = epoca
    };
    saludar(){
        return `Hola ${this.nombre}, fracasado!. Te vi una vez ${this.epoca}.`;
    }

}

//Creo una instancia de esta nueva clase
let fracasado = new JugadorFracasado('Javier Castrillo', 'El último 11', 'en los 80');


// Ya heredo los métodos
console.log(fracasado.mostrar());

// Y uso el nuevo
console.log(fracasado.saludar());


// Importación y exportación
// Voy a importar un módulo
// por favor fijarse en la línea 1 de este archivo, la llamada
// de la importación. Se tiene que poner arriba, no en cualquier lado
// y también fijarse la línea 12 del index.html en la que le hice el agregado
// que es un módulo

// Acá imprimo la variable que traje con el mismo nombre

console.log(materia);

// Y acá importé el default, por eso puedo poner el nombre que quiera

console.log(miFacu);

// Imprimo el objeto importado

console.log(diaClase);

// Uso una función importada

const anioActual = recordar("2020", "pandemia");
console.log(anioActual);

// Y una clase también

const auto1 = new Auto("Ferrari", "Italia");
console.log(auto1);


// Y por supuesto una clase con método

const moto1 = new Moto("Ducati", "Italia");
const salida = moto1.describir();
console.log(salida);
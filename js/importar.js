// Yo puedo exportar cosas de un archivo js a otro
// Por ejemplo una variable
// y la importo con el mismo nombre en otro js

export const materia = "Construcción de Interfases de Usuario";

// También hay una exportación por default
// que no requiere mantener el nombre

const universidad = "Universidad Nacional de Hurlingham";
export default universidad;

// Claro que puedo exportar un objeto

export const diaClase = {
    dia: "Jueves",
    hora: "18:00"
};


// Y una función entera

export const recordar = (anio, circunstancia) => {
    return `El año ${anio} será recordado por ${circunstancia}`;
};

// Y por supuesto una clase también!

export class Auto { constructor (marca, pais) {
    this.marca = marca;
    this.pais = pais
}
};

// Y por supuesto la clase con método 

export class Moto { constructor (marca, pais) {
    this.marca = marca;
    this.pais= pais
}
describir() {
    return `La moto ${this.marca} es originaria de ${this.pais}`;
}
};